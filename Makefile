##
## ./Makefile for CV
##
## Made by Karl Toffel
## Login   <kapnoc@memeware.net>
##
## Started on  Tue Aug 22 18:06:29 2017 Karl Toffel
## Last update Tue Apr 17 13:32:03 2018 Tanguy Gérôme
##

NAME	=	CV-tanguy.gerome

MAIN	=	main

LC	=	echo | pdflatex

all:	en fr fi

en:
	$(LC) --jobname="$(NAME).$@" "\AtBeginDocument{\$@true}\input{$(MAIN)}"
	# $(LC) --jobname="$(NAME).$@" "\AtBeginDocument{\$@true}\input{$(MAIN)}"
fr:
	$(LC) --jobname="$(NAME).$@" "\AtBeginDocument{\$@true}\input{$(MAIN)}"
	# $(LC) --jobname="$(NAME).$@" "\AtBeginDocument{\$@true}\input{$(MAIN)}"
fi:
	$(LC) --jobname="$(NAME).$@" "\AtBeginDocument{\$@true}\input{$(MAIN)}"
	# $(LC) --jobname="$(NAME).$@" "\AtBeginDocument{\$@true}\input{$(MAIN)}"

clean:
	./clean

fclean:	clean
	rm -f $(NAME).en.pdf
	rm -f $(NAME).fr.pdf
	rm -f $(NAME).fi.pdf

.PHONY:	all en fr fi clean fclean
